package br.com.rpgbattle.utils;

import org.springframework.stereotype.Component;

@Component
public class GenerateRandomImpl implements GenerateRandom {

	public int getRandom(int max) {
		return (int) (Math.random() * max);
	}

	public int getRandomInteger(int maximum, int minimum) {
		return ((int) (Math.random() * (maximum - minimum))) + minimum;
	}

}