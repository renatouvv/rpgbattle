package br.com.rpgbattle.utils;

public interface GenerateRandom {
	
	public int getRandom(int max);

	public int getRandomInteger(int maximum, int minimum);

}