package br.com.rpgbattle;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import br.com.rpgbattle.controller.RpgBattleController;

@SpringBootApplication
public class RpgBattleApplication {
	
	@Bean
	ResourceConfig resourceConfig() {
		return new ResourceConfig().register(RpgBattleController.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(RpgBattleApplication.class, args);
	}

}