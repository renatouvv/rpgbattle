package br.com.rpgbattle.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.rpgbattle.model.BattleHistory;
import br.com.rpgbattle.model.BattleResult;
import br.com.rpgbattle.model.JsonCharSelection;
import br.com.rpgbattle.model.Person;
import br.com.rpgbattle.model.User;
import br.com.rpgbattle.service.PersonService;

@Controller
@Path("/battle")
public class RpgBattleController {

	private static final Logger logger = LoggerFactory.getLogger(RpgBattleController.class);

	@Autowired
	private PersonService service;

	@Path("/characters")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<List<Person>> getAll() {
		System.out.println("Entrou no método recuperar personagens");
		List<Person> characters = service.getAllCharacters();
		return new ResponseEntity<List<Person>>(characters, HttpStatus.OK);
	}

	@Path("/characters/add")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseEntity<Person> add(String json) {
		Person character;
		ResponseEntity<Person> response;
		ObjectMapper mapper = new ObjectMapper();

		try {
			character = mapper.readValue(json, Person.class);
			character = service.addNewCharacter(character);
			response = new ResponseEntity<Person>(character, HttpStatus.OK);
		} catch (Exception e) {
			response = new ResponseEntity<Person>(new Person(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error("Ocorreu um erro ao tentar adicionar um personagem." + "\n Mensagem: " + e.getMessage()
					+ "\n Causa: " + e.getCause());
		}

		return response;
	}

	@Path("/characters/find/name")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Person> findByName(@QueryParam(value = "name") String name) {
		Person character;
		ResponseEntity<Person> response;

		try {
			character = service.getCharacterByName(name);
			response = new ResponseEntity<Person>(character, HttpStatus.OK);
		} catch (Exception e) {
			response = new ResponseEntity<Person>(new Person(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error("Ocorreu um erro ao consultar o personagem: " + name + "\n Mensagem: " + e.getMessage()
					+ "\n Causa: " + e.getCause());
		}
		return response;
	}
	
	@Path("/history/find")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<List<BattleHistory>> findHistoryByUser(@QueryParam(value = "nickname") String nickname) {
		ResponseEntity<List<BattleHistory>> response;
		List<BattleHistory> history = new ArrayList<BattleHistory>();
		history = service.getBattleHistoriesByUser(nickname);
		if (history.size() > 0) {
			response = new ResponseEntity<List<BattleHistory>>(history, HttpStatus.OK);
		} else {
			response = new ResponseEntity<List<BattleHistory>>(history, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return response;
	}

	@Path("/start/flow")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<BattleResult> startBattleFlow(String json) {

		JsonCharSelection selectedCharacter;
		Person hero;
		User user;
		BattleResult battleResult = new BattleResult();
		ResponseEntity<BattleResult> response;
		ObjectMapper mapper = new ObjectMapper();

		try {
			// receber o nickname e herói selecionado
			selectedCharacter = mapper.readValue(json, JsonCharSelection.class);
			String nickname = selectedCharacter.getNickname();
			String selectedHero = selectedCharacter.getCharName();
			
			// verifica se já existe um nickname, caso contrário persiste o usuário
			user = service.addNewUser(nickname);
			
			// pega dados do herói selecionado no banco
			hero = service.getCharacterByName(selectedHero);
			
			//adicionar dados do usuário ao resultado da batalha
			
			battleResult = service.processBattle(user, hero, battleResult);

			response = new ResponseEntity<BattleResult>(battleResult, HttpStatus.OK);
			
		} catch (Exception e) {
			logger.error("Ocorreu um erro ao processar a batalha!" + "\nMensagem: " + e.getMessage() + "\nCausa: "
					+ e.getCause());
			response = new ResponseEntity<BattleResult>(new BattleResult(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return response;

	}

}