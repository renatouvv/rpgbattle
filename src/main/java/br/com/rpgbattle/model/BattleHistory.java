package br.com.rpgbattle.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class BattleHistory {
	
	@Id
	private String id;
	
	private User user;
	
	private Person hero;
	
	private Person monster;
	
	private List<Turn> turns;
	
	public String getId() {
		return id;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Person getHero() {
		return hero;
	}

	public void setHero(Person hero) {
		this.hero = hero;
	}

	public Person getMonster() {
		return monster;
	}

	public void setMonster(Person monster) {
		this.monster = monster;
	}

	public List<Turn> getTurns() {
		return turns;
	}

	public void setTurns(List<Turn> turns) {
		this.turns = turns;
	}

}