package br.com.rpgbattle.model;

public class JsonCharSelection {
	
	private String nickname;
	
	private String charName;

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getCharName() {
		return charName;
	}

	public void setCharName(String charName) {
		this.charName = charName;
	}

}