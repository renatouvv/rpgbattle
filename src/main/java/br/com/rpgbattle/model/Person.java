package br.com.rpgbattle.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import br.com.rpgbattle.enums.PersonType;

@Document
public class Person {

	@Id
	private String id;
	
	private PersonType type;
	
	private String name;
	
	private int pointsOfLife;
	
	private int strength;
	
	private int defense;
	
	private int agility;
	
	private String damageFactor;
	
	public String getId() {
		return id;
	}

	public PersonType getType() {
		return type;
	}

	public void setType(PersonType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPointsOfLife() {
		return pointsOfLife;
	}

	public void setPointsOfLife(int pointsOfLife) {
		this.pointsOfLife = pointsOfLife;
	}

	public int getStrength() {
		return strength;
	}

	public void setStrength(int strength) {
		this.strength = strength;
	}

	public int getDefense() {
		return defense;
	}

	public void setDefense(int defense) {
		this.defense = defense;
	}

	public int getAgility() {
		return agility;
	}

	public void setAgility(int agility) {
		this.agility = agility;
	}

	public String getDamageFactor() {
		return damageFactor;
	}

	public void setDamageFactor(String damageFactor) {
		this.damageFactor = damageFactor;
	}
	
}