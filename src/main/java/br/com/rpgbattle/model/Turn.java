package br.com.rpgbattle.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Turn {
	
	@Id
	private String id;
	
	private int heroInitiative;
	
	private int monsterInitiative;
	
	private int heroAttack;
	
	private int monsterAttack;
	
	private int heroDamage;
	
	private int monsterDamage;
	
	public String getId() {
		return id;
	}

	public int getHeroInitiative() {
		return heroInitiative;
	}

	public void setHeroInitiative(int heroInitiative) {
		this.heroInitiative = heroInitiative;
	}

	public int getMonsterInitiative() {
		return monsterInitiative;
	}

	public void setMonsterInitiative(int monsterInitiative) {
		this.monsterInitiative = monsterInitiative;
	}

	public int getHeroAttack() {
		return heroAttack;
	}

	public void setHeroAttack(int heroAttack) {
		this.heroAttack = heroAttack;
	}

	public int getMonsterAttack() {
		return monsterAttack;
	}

	public void setMonsterAttack(int monsterAttack) {
		this.monsterAttack = monsterAttack;
	}

	public int getHeroDamage() {
		return heroDamage;
	}

	public void setHeroDamage(int heroDamage) {
		this.heroDamage = heroDamage;
	}

	public int getMonsterDamage() {
		return monsterDamage;
	}

	public void setMonsterDamage(int monsterDamage) {
		this.monsterDamage = monsterDamage;
	}

}