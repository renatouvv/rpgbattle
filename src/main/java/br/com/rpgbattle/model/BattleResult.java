package br.com.rpgbattle.model;

public class BattleResult {
	
	private User userData;
	
	private Person hero;
	
	private Person monster;
	
	private int turns;
	
	private String winner;

	public User getUserData() {
		return userData;
	}

	public void setUserData(User userData) {
		this.userData = userData;
	}

	public Person getHero() {
		return hero;
	}

	public void setHero(Person hero) {
		this.hero = hero;
	}

	public Person getMonster() {
		return monster;
	}

	public void setMonster(Person monster) {
		this.monster = monster;
	}

	public int getTurns() {
		return turns;
	}

	public void setTurns(int turns) {
		this.turns = turns;
	}

	public String getWinner() {
		return winner;
	}

	public void setWinner(String winner) {
		this.winner = winner;
	}
	
}