package br.com.rpgbattle.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.rpgbattle.enums.PersonType;
import br.com.rpgbattle.model.BattleHistory;
import br.com.rpgbattle.model.BattleResult;
import br.com.rpgbattle.model.Person;
import br.com.rpgbattle.model.Turn;
import br.com.rpgbattle.model.User;
import br.com.rpgbattle.repository.BattleHistoryRepository;
import br.com.rpgbattle.repository.PersonRepository;
import br.com.rpgbattle.repository.UserRepository;
import br.com.rpgbattle.utils.GenerateRandom;

@Service
public class PersonServiceImpl implements PersonService {

	private static final Logger logger = LoggerFactory.getLogger(PersonServiceImpl.class);

	@Autowired
	private PersonRepository repo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private BattleHistoryRepository historyRepo;
	
	@Autowired
	private GenerateRandom random;

	@Override
	public List<Person> getAllCharacters() {
		List<Person> characters = repo.findAll();
		return characters;
	}

	@Override
	public Person getRandomCharacter() {
		int size = repo.countPersonByType(PersonType.Monstro);
		int draw = random.getRandomInteger(size, 0);
		logger.info("Random monster: " + draw);

		List<Person> characters = repo.findPersonByType(PersonType.Monstro);

		return characters.get(draw);
	}

	@Override
	public Person addNewCharacter(Person character) {
		return repo.save(character);
	}
	
	@Override
	public User addNewUser(String nickname) {
		User user = null;
		user = userRepo.findUserByNickname(nickname);
		if (null != user) {
			return user;
		} else {
			user = new User();
			user.setNickname(nickname);
			user.setRank(0);
			return userRepo.save(user);
		}
	}

	@Override
	public Person getCharacterByName(String name) {
		return repo.findPersonByName(name);
	}

	@Override
	public List<Person> getCharacterByType(PersonType type) {
		return repo.findPersonByType(type);
	}
	
	@Override
	public List<BattleHistory> getBattleHistoriesByUser(String nickname) {
		List<BattleHistory> history = historyRepo.findAll();
		
		for (int i = 0; i < history.size(); i++) {
			if (!history.get(i).getUser().getNickname().equals(nickname)) {
				history.remove(i);
				i--;
			}
		}
		
		return history;
	}

	@Override
	public BattleResult processBattle(User user, Person hero, BattleResult battleResult) {
		
		String battleResultMsg = "";
		List<Turn> turns = new ArrayList<Turn>();
		BattleHistory history = new BattleHistory();
		history.setUser(user);
		history.setHero(hero);
		
		// sortear o monstro
		Person monster = this.getRandomCharacter();
		history.setMonster(monster);
		
		//Adiciona o herói e o monstro ao resultado da batalha
		battleResult.setUserData(user);
		battleResult.setHero(hero);
		battleResult.setMonster(monster);
		
		logger.info(" ##### Herói: " + hero.getName() + "#####");
		logger.info(" ##### Monstro: " + monster.getName() + "#####");

		// ### iniciar a batalha ###
		int shifts = 0;
		while (hero.getPointsOfLife() > 0 || monster.getPointsOfLife() > 0) {
			
			Turn turn = new Turn();
			
			// rolar iniciativa
			int initiative = this.initiativeRoll(hero.getAgility(), monster.getAgility(), turn);

			// Herói ataca primeiro			
			if (initiative == 0) {
				
				int attackResult = this.AttackVsDefenseRoll(hero, monster, turn);

				// Cálculo do Dano
				if (attackResult == 0) {
					
					int damage = this.damageRoll(hero, turn);

					monster.setPointsOfLife(monster.getPointsOfLife() - damage);

					if (monster.getPointsOfLife() <= 0) {
						logger.info("Luta encerrada. Herói saiu vitorioso!");
						battleResultMsg = "O herói " + hero.getName() + " foi o vencedor desta batalha!";
						int ranking = this.rankCalculation(shifts);
						user.setRank(user.getRank() + ranking);
						userRepo.save(user);
						break;
					} else {
						logger.info("Uma nova rodada será iniciada!");
						shifts++;
						turns.add(turn);
					}
				} else {
					logger.info("O monstro resistiu ao dano infligido pelo herói!");
					shifts++;
				}

			} else {
				// Monstro ataca primeiro
				logger.info("Monstro inicia o ataque!");
				
				int attackResult = this.AttackVsDefenseRoll(monster, hero, turn);

				// Cálculo do Dano
				if (attackResult == 0) {
					
					int damage = this.damageRoll(monster, turn);

					hero.setPointsOfLife(hero.getPointsOfLife() - damage);

					if (hero.getPointsOfLife() <= 0) {
						logger.info("Luta encerrada. Monstro saiu vitorioso!");
						battleResultMsg = "O monstro " + monster.getName() + " foi o vencedor desta batalha!";
						break;
					} else {
						logger.info("Uma nova rodada será iniciada!");
						shifts++;
						turns.add(turn);
					}
				} else {
					logger.info("O herói resistiu ao dano infligido pelo monstro!");
					shifts++;
				}
			}

		}
		
		history.setTurns(turns);
		
		// Persiste histórico no banco
		historyRepo.save(history);
		
		battleResult.setTurns(shifts);
		battleResult.setWinner(battleResultMsg);
		
		return battleResult;

	}
	
	/*
	 * Retorna 0 ou 1 
	 * se o herói tiver iniciativa superior retorna 0
	 * se o monstro tiver iniciativa superior retorna 1
	 */
	private int initiativeRoll(int heroAgility, int monsterAgility, Turn turn) {
		
		int result;
		
		int heroInitiative = random.getRandomInteger(10, 1) + heroAgility;
		int monsterInitiative = random.getRandomInteger(10, 1) + monsterAgility;
		logger.info("Iniciativa do Herói: " + heroInitiative);
		logger.info("Iniciativa do Monstro: " + monsterInitiative);
		
		turn.setHeroInitiative(heroInitiative);
		turn.setMonsterInitiative(monsterInitiative);
		
		if (heroInitiative > monsterInitiative) {
			result = 0;
		} else {
			result = 1;
		}
		
		return result;
	}
	
	/*
	 * Retorna 0 ou 1 
	 * se o atacante superar a defesa retorna 0
	 * Caso contrário retorna 1
	 */
	private int AttackVsDefenseRoll(Person attacker, Person defender, Turn turn) {
		
		int result;
		
		logger.info("O " + attacker.getType() + " inicia o ataque.");
		int attack = random.getRandomInteger(10, 1) + attacker.getAgility() + attacker.getStrength();
		int defense = random.getRandomInteger(10, 1) + defender.getAgility() + defender.getDefense();
		logger.info("Rolamento do ataque do Atacante " + attacker.getType() + " " + attacker.getName() + ": " + attack);
		logger.info("Rolamento da defesa do Defensor " + defender.getType() + " " + defender.getName() + ": " + defense);
		
		if (attacker.getType().equals(PersonType.Herói)) {
			turn.setHeroAttack(attack);
		} else {
			turn.setMonsterAttack(attack);
		}
		
		if (attack > defense) {
			result = 0;
		} else {
			result = 1;
		}
		
		return result;
	}
	
	/*
	 * Retorna o valor do dano
	 * a ser debitado do defensor
	 */
	private int damageRoll(Person attacker, Turn turn) {

		logger.info("Ataque do " + attacker.getType() + " " + attacker.getName() + " superou a defesa adversário.");
		String damageFactor = attacker.getDamageFactor();
		logger.info("Fator de dano do " + attacker.getType() + " " + attacker.getName() + " " + damageFactor);
		int diceQuantity = Integer.valueOf(damageFactor.substring(0, 1));
		logger.info("Quantidade de dados: " + diceQuantity);
		int faces = Integer.valueOf(damageFactor.substring(2, 3));
		logger.info("Número de faces: " + faces);

		int damage = 0;

		for (int i = 0; i < diceQuantity; i++) {
			damage = damage + random.getRandomInteger(faces, 1);
		}

		damage = damage + attacker.getStrength();
		logger.info("Rolamento do dano do Atacante " + attacker.getType() + " " + attacker.getName() + " ao defensor: " + damage);
		
		if (attacker.getType().equals(PersonType.Herói)) {
			turn.setHeroDamage(damage);
		} else {
			turn.setMonsterDamage(damage);
		}
		
		return damage;
		
	}

	/*
	 * Cálculo do ranking do usuário
	 */
	private int rankCalculation(int shifts) {
		return 100 - shifts;
	}

}