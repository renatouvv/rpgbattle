package br.com.rpgbattle.service;

import java.util.List;

import br.com.rpgbattle.enums.PersonType;
import br.com.rpgbattle.model.BattleHistory;
import br.com.rpgbattle.model.BattleResult;
import br.com.rpgbattle.model.Person;
import br.com.rpgbattle.model.User;

public interface PersonService {

	public List<Person> getAllCharacters();
	
	public Person getRandomCharacter();

	public Person addNewCharacter(Person character);
	
	public User addNewUser(String nickname);
	
	public Person getCharacterByName(String name);
	
	public List<Person> getCharacterByType(PersonType type);
	
	public List<BattleHistory> getBattleHistoriesByUser(String nickname);
	
	public BattleResult processBattle(User user, Person hero, BattleResult battleResult);
	
}