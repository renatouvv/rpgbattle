package br.com.rpgbattle.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import br.com.rpgbattle.model.User;

@Transactional
public interface UserRepository extends MongoRepository<User, Long> {
	
	public User findUserByNickname(String nickname);

}