package br.com.rpgbattle.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import br.com.rpgbattle.enums.PersonType;
import br.com.rpgbattle.model.Person;

@Transactional
public interface PersonRepository extends MongoRepository<Person, Long> {

	public Person findPersonByName(String name);
	
	public List<Person> findPersonByType(PersonType type);
	
	public int countPersonByType(PersonType type);
	
}