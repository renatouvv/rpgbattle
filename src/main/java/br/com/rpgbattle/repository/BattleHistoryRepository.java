package br.com.rpgbattle.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import br.com.rpgbattle.model.BattleHistory;

@Transactional
public interface BattleHistoryRepository extends MongoRepository<BattleHistory, Long> {
	
}