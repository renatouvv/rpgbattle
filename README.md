# RPG Battle Challenge
> This project is a API that executes a round based RPG battle.

This is an epic round based RPG battle where a user select one hero to fight against one big bad monster.

## Usage example

Here goes a few examples of how the application can be used.

You can use Postman to call the requests as indicated below.

First of all, we created a request to add heroes and monster to our application:


```text
Url: http://localhost:8080/api/battle/characters/add 
Method: POST 
Request: json
```



```json
{
    "type": "Herói",
    "name": "Guerreiro",
    "pointsOfLife": 12,
    "strength": 4,
    "defense": 3,
    "agility": 3,
    "damageFactor": "2d4"
}
```

Response example:


```json
{
    "headers": {},
    "body": {
        "id": "5f21cfecd0cd42497363e523",
        "type": "Herói",
        "name": "Elf",
        "pointsOfLife": 18,
        "strength": 3,
        "defense": 2,
        "agility": 5,
        "damageFactor": "2d6"
    },
    "statusCode": "OK",
    "statusCodeValue": 200
}
```

Ok, now you can experience the EPIC RPG Battle.

So you can call the next request, the one that makes the magic happen!


```text
Url: http://localhost:8080/api/battle/start/flow
Method: POST
Request: json
```



```json
{
    "nickname": "zedascouves",
    "charName": "Bárbaro"
}
```

Response example:


```json
{
    "headers": {},
    "body": {
        "userData": {
            "id": "5f21bfd1149d91698b42cd49",
            "nickname": "rdpaula",
            "rank": 97
        },
        "hero": {
            "id": "5f1f47e881e409397d58f983",
            "type": "Herói",
            "name": "Bárbaro",
            "pointsOfLife": 4,
            "strength": 6,
            "defense": 1,
            "agility": 3,
            "damageFactor": "2d6"
        },
        "monster": {
            "id": "5f1f484c81e409397d58f985",
            "type": "Monstro",
            "name": "Morto-Vivo",
            "pointsOfLife": -13,
            "strength": 4,
            "defense": 0,
            "agility": 1,
            "damageFactor": "2d4"
        },
        "turns": 3,
        "winner": "O herói Bárbaro foi o vencedor desta batalha!"
    },
    "statusCode": "OK",
    "statusCodeValue": 200
}
```

And if you want to consult all the history by your own or others nicknames, you can do it!!!

```text
Url: http://localhost:8080/api/battle/history/find?nickname=zedascouves
Method: GET
Request: Param (nickname = <<yournickname>>
```

Response example:


```json
{
    "headers": {},
    "body": [
        {
            "id": "5f21ded9638bb3750c5ed767",
            "user": {
                "id": "5f21ded9638bb3750c5ed766",
                "nickname": "zedascouves",
                "rank": 0
            },
            "hero": {
                "id": "5f1f481e81e409397d58f984",
                "type": "Herói",
                "name": "Paladino",
                "pointsOfLife": -5,
                "strength": 2,
                "defense": 5,
                "agility": 1,
                "damageFactor": "2d4"
            },
            "monster": {
                "id": "5f1f487581e409397d58f986",
                "type": "Monstro",
                "name": "Orc",
                "pointsOfLife": 16,
                "strength": 6,
                "defense": 2,
                "agility": 2,
                "damageFactor": "1d8"
            },
            "turns": [
                {
                    "id": null,
                    "heroInitiative": 8,
                    "monsterInitiative": 7,
                    "heroAttack": 9,
                    "monsterAttack": 0,
                    "heroDamage": 4,
                    "monsterDamage": 0
                },
                {
                    "id": null,
                    "heroInitiative": 3,
                    "monsterInitiative": 4,
                    "heroAttack": 0,
                    "monsterAttack": 16,
                    "heroDamage": 0,
                    "monsterDamage": 11
                }
            ]
        }
    ],
    "statusCode": "OK",
    "statusCodeValue": 200
}
```

## Development setup

This is an Spring Boot application with MongoDB.

To run this application you will have to clone this project and have mongoDB installed.

We are using:


```text
	Java 8
	MongoDB 4.2
	Jax-RS
```

## Release History

* 0.0.1
    * Work in progress

## Meta

>Renato de Paula – renatouvv@gmail.com